#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )"

IDX="${1}"


source "${DIR}/00-ltoggle-common.sh"


__winidcaptured "${IDX}"  &&  wmctrl -i -a "$(__winidfromfile "${IDX}")"

