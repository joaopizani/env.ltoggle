#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

IDX="${1}"


source "${DIR}/00-ltoggle-common.sh"


__recordactivewinid() { __recordwinid "${1}" "$(xdotool getwindowfocus)"; }
__recordactivewinid "${IDX}"
