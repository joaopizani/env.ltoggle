#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )"

TIMEOUT_CNT_MAX=100  # 20s
TIMEOUT_CNT_STEP="0.2s"

IDX="${1}"


source "${DIR}/00-ltoggle-common.sh"

SPLASHSKIP="${LTMAP_SPLASHSKIP["${IDX}"]}"

CMDNAME_VAR="LTMAP_CMD_${IDX}[0]"
SEARCHNAME_DEFAULT="${!CMDNAME_VAR}"
SEARCHNAME_EXPLICIT="${LTMAP_SEARCHNAME["${IDX}"]}"
SEARCHNAME="${SEARCHNAME_EXPLICIT:-"${SEARCHNAME_DEFAULT}"}"


ARNAME="LTMAP_CMD_${IDX}[@]"
ARCMD="${!ARNAME}"

I3LAYOUTSCR="${XDG_CONFIG_HOME:-"${HOME}/.config"}/i3/layout/assign.sh"


if [ -n "${ARCMD}" ]; then
    </dev/null "${!ARNAME}" &>/dev/null  &  disown

    # Try to record "pre-state" only AFTER an eventual splash window, so we consider real only what's AFTER that
    if [[ -n "${SPLASHSKIP}" ]]; then sleep "${SPLASHSKIP}"; fi
    IDSPRE="$(xdotool search --onlyvisible "${SEARCHNAME}" 2>/dev/null)"

    # Loop with timeout waiting for a new window id with correct instance name to show up
    TIMEOUT_CNT=0
    while [[ $TIMEOUT_CNT -lt $TIMEOUT_CNT_MAX ]]; do
        IDSPOST="$(xdotool search --onlyvisible "${SEARCHNAME}" 2>/dev/null)"
        IDSNEW="$( ( { if [[ -n "${IDSPRE}" ]]; then echo "${IDSPRE}"; fi } ; echo "${IDSPOST}" ) | awk NF | sort | uniq -u | head -n 1)"

        if [[ -n "${IDSNEW}" ]]; then
            __recordwinid "${IDX}" "${IDSNEW}"

            if [ -s "${I3LAYOUTSCR}" ]; then  "${I3LAYOUTSCR}" "${IDX}" "${IDSNEW}";  fi  # If I3 layout feature available

            "${DIR}/ltoggle-toggle.sh" "${IDX}"
            break
        fi

        ((++TIMEOUT_CNT))
        sleep "${TIMEOUT_CNT_STEP}"
    done

fi

