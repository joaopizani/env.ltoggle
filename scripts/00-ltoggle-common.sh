LTMAP_CMD_0=('x-terminal-emulator' '-e' 'bash' '-i' '-c' 'screen -xRR primary; exec bash')
LTMAP_SEARCHNAME[0]="mate-terminal"

LTMAP_CMD_1=('x-www-browser')
LTMAP_SEARCHNAME[1]="Navigator"

LTMAP_CMD_2=('x-www-browser' '-private-window')
LTMAP_SEARCHNAME[2]="Navigator"

LTMAP_CMD_3=('xdg-open' "$(xdg-user-dir DOWNLOAD)")
LTMAP_SEARCHNAME[3]="caja"

LTMAP_CMD_4=('bitwarden')
LTMAP_SEARCHNAME[4]="bitwarden"

LTMAP_CMD_5=('evolution')
LTMAP_SEARCHNAME[5]="evolution"

LTMAP_CMD_6=('cantata')
LTMAP_SEARCHNAME[6]="cantata"



OVERRIDE_DIR="${XDG_CONFIG_HOME:-"${HOME}/.config"}/ltoggle"
OVERRIDE_FILE="${OVERRIDE_DIR}/00-ltoggle-override.sh"
mkdir -p "${OVERRIDE_DIR}";    [ -s "${OVERRIDE_FILE}" ]  &&  source "${OVERRIDE_FILE}"


ID_DIR="${XDG_RUNTIME_DIR:-"/run/user/$(id -u)"}/ltoggle"
ID_PREFIX="${ID_DIR}/ltoggle-winid"
mkdir -p "${ID_DIR}"


__winidcaptured() { test -s "${ID_PREFIX}-${1}"; }
__winidfromfile() { cat "${ID_PREFIX}-${1}"; }

__recordwinid() { echo "${2}" > "${ID_PREFIX}-${1}"; }
