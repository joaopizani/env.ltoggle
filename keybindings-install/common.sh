SCHEMA_STR_=""; for p in ${SCHEMA_ARR[@]}; do SCHEMA_STR_+="${p}."; done
PATH_STR_="/"; for p in ${PATH_ARR[@]}; do PATH_STR_+="${p}/"; done

SCHEMA_STR="${SCHEMA_STR_}${SS}"
PATH_STR="${PATH_STR_}${SS}s"


source "${DIRUP}/keybindings.sh"


GRPNAME="ltoggle"
PREFIX="zzz-${GRPNAME}"

if [[ "${1}" == "reset" ]]; then

    for ix in "${!LTMAP_KEYB_TOGGLE[@]}"; do gsettings reset-recursively "${SCHEMA_STR}:${PATH_STR}/${PREFIX}-toggle-${ix}/"; done
    for ix in "${!LTMAP_KEYB_CAPTURE[@]}"; do gsettings reset-recursively "${SCHEMA_STR}:${PATH_STR}/${PREFIX}-capture-${ix}/"; done
    for ix in "${!LTMAP_KEYB_LAUNCH[@]}"; do gsettings reset-recursively "${SCHEMA_STR}:${PATH_STR}/${PREFIX}-launch-${ix}/"; done

else

    for ix in "${!LTMAP_KEYB_TOGGLE[@]}"; do
        SETT="toggle-${ix}"
        gsettings set "${SCHEMA_STR}:${PATH_STR}/${PREFIX}-${SETT}/" "name"    "'ltoggle-${SETT}'"
        gsettings set "${SCHEMA_STR}:${PATH_STR}/${PREFIX}-${SETT}/" "action"  "'ltoggle-toggle.sh ${ix}'"
        gsettings set "${SCHEMA_STR}:${PATH_STR}/${PREFIX}-${SETT}/" "binding" "'${LTMAP_KEYB_TOGGLE["${ix}"]}'"
    done
    for ix in "${!LTMAP_KEYB_CAPTURE[@]}"; do
        SETT="capture-${ix}"
        gsettings set "${SCHEMA_STR}:${PATH_STR}/${PREFIX}-${SETT}/" "name"    "'ltoggle-${SETT}'"
        gsettings set "${SCHEMA_STR}:${PATH_STR}/${PREFIX}-${SETT}/" "action"  "'ltoggle-capture.sh ${ix}'"
        gsettings set "${SCHEMA_STR}:${PATH_STR}/${PREFIX}-${SETT}/" "binding" "'${LTMAP_KEYB_CAPTURE["${ix}"]}'"
    done
    for ix in "${!LTMAP_KEYB_LAUNCH[@]}"; do
        SETT="launch-${ix}"
        gsettings set "${SCHEMA_STR}:${PATH_STR}/${PREFIX}-${SETT}/" "name"    "'ltoggle-${SETT}'"
        gsettings set "${SCHEMA_STR}:${PATH_STR}/${PREFIX}-${SETT}/" "action"  "'ltoggle-launch.sh ${ix}'"
        gsettings set "${SCHEMA_STR}:${PATH_STR}/${PREFIX}-${SETT}/" "binding" "'${LTMAP_KEYB_LAUNCH["${ix}"]}'"
    done

fi

