#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )"


RESETPARAM="${1}"

SCHEMA_ARR=("org" "gnome" "settings-daemon" "plugins" "media-keys")
PATH_ARR=("org" "gnome" "settings-daemon" "plugins" "media-keys")
SS="custom-keybinding"


DIRUP="${DIR}/.."
source "${DIRUP}/common.sh" "${RESETPARAM}"



SCHEMA_STR_TRUNCATED="${SCHEMA_STR_:0:${#SCHEMA_STR_} - 1}"
KEYLIST_EXISTING="$(gsettings get "${SCHEMA_STR_TRUNCATED}" "${SS}s")"

KEYLIST_ADD="'${PATH_STR}/yyy-mediacenter-empty/'"
for nam in "${!QUICKACT_BINDINGS[@]}"; do KEYLIST_ADD+=",'${PATH_STR}/yyy-${nam}/'"; done
KEYLIST_ADD+="]"

[[ ${KEYLIST_EXISTING:(-2)} = "[]" ]] && KEYLIST="[${KEYLIST_ADD}" || KEYLIST="${KEYLIST_EXISTING:0:${#KEYLIST_EXISTING} - 1},${KEYLIST_ADD}"
gsettings set "${SCHEMA_STR_TRUNCATED}" "${SS}s" "${KEYLIST}"

