#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )"


RESETPARAM="${1}"

SCHEMA_ARR=("org" "mate" "control-center")
PATH_ARR=("org" "mate" "desktop")
SS="keybinding"



DIRUP="${DIR}/.."
source "${DIRUP}/common.sh" "${RESETPARAM}"

