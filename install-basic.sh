#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )"

SYSD_USER_BINARIES="$(systemd-path user-binaries)"
BINDIR="${SYSD_USER_BINARIES:-"${HOME}/.local/bin"}"

SCRIPTSDIR="${DIR}/scripts"
SCRIPTS_TO_INSTALL=( 'ltoggle-launch.sh' 'ltoggle-toggle.sh' 'ltoggle-capture.sh' )

mkdir -p "${BINDIR}"
for s in ${SCRIPTS_TO_INSTALL[@]}; do ln -sfn "${SCRIPTSDIR}/${s}" "${BINDIR}/"; done

